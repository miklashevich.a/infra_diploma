data "aws_availability_zones" "available" {}

data "aws_ami" "ubuntu" {
  owners      = var.ami_owners
  most_recent = true
  filter {
    name   = var.ami_filter_name
    values = var.ami_filter_values
  }
}


resource "aws_security_group" "web" {
  name = var.aws_security_group_name

  dynamic "ingress" {
    for_each = var.allow_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = var.aws_security_group_ingress_protocol
      cidr_blocks = var.security_group_ingress_cidr_blocks
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = var.aws_security_group_egress_protocol
    cidr_blocks = var.security_group_egress_cidr_blocks
  }

  tags = {
    Name  = var.aws_security_group_tag_name
    CM   = "consul"
  }
}

# My initial AMI Launch Configuration for APP Instance
resource "aws_launch_configuration" "web_initial" {
  name_prefix       = "APP"
  image_id          = data.aws_ami.ubuntu.id
  instance_type     = var.instance_type
  security_groups   = [aws_security_group.web.id]
  user_data         = file("user_data.sh")
  key_name          = var.ssh_key_name
  lifecycle {
    create_before_destroy = true
  }

}

# My custom AMI Launch Configuration for APP Instances
  resource "aws_launch_configuration" "web_custom" {
  name              = "APP"
  image_id          = "ami-0377ee517c34fcb41"  # golden image custom AMI ID
  instance_type     = var.instance_type
  security_groups   = [aws_security_group.web.id]
  user_data         = file("deploy.sh")
  key_name          = var.ssh_key_name

  iam_instance_profile = "arn:aws:iam::233508515956:instance-profile/ec2Full"


  lifecycle {
    create_before_destroy = true

  }
}


resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web_initial.name}" # change to .web_custom.name (after initial launch)
  launch_configuration = aws_launch_configuration.web_custom.name          # change to .web_custom.name (after initial launch) 
  min_size             = var.autoscaling_group_min_size
  max_size             = var.autoscaling_group_max_size
  min_elb_capacity     = var.autoscaling_group_min_elb_capacity
  health_check_type    = "EC2"
  health_check_grace_period = "300"
  vpc_zone_identifier  = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]

  dynamic "tag" {
    for_each = {
      Name = var.autoscaling_group_dynamic_tag_name
      CM   = "consul"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_policy" "scale_up" {
  name                   = "scale-up"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}

resource "aws_autoscaling_policy" "scale_down" {
  name                   = "scale-down"
  scaling_adjustment    = -1
  adjustment_type       = "ChangeInCapacity"
  cooldown              = 300
  autoscaling_group_name = aws_autoscaling_group.web.name
}


resource "aws_cloudwatch_metric_alarm" "cpu_utilization_alarm" {
  alarm_name          = "cpu-utilization-alarm"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  threshold           = "95"
  alarm_description   = "Scale up if CPU utilization is greater than threshold or equal"
  statistic           = "Average"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web.name
  }

  alarm_actions = [aws_autoscaling_policy.scale_up.arn]

  insufficient_data_actions = []
}

#resource "aws_cloudwatch_metric_alarm" "cpu_utilization_scale_down_alarm" {
  #alarm_name          = "cpu-utilization-scale-down-alarm"
  #comparison_operator = "LessThanOrEqualToThreshold"
  #evaluation_periods  = "1"
  #metric_name         = "CPUUtilization"
  #namespace           = "AWS/EC2"
  #period              = "600"
  #threshold           = "2"  # Set to a value 
  #alarm_description   = "Scale down if CPU utilization is less than threshold"
  #statistic           = "Average"

  #dimensions = {
    #AutoScalingGroupName = aws_autoscaling_group.web.name
  #}

  #alarm_actions = [aws_autoscaling_policy.scale_down.arn]

  #insufficient_data_actions = []
#}



resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}


data "aws_route53_zone" "selected" {
    name = var.domain_name 
}


resource "aws_route53_record" "www" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.domain_name
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.consul-template_server.public_ip]
}   


resource "aws_route53_record" "monitoring" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.monitoring_sub_domain
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.monitoring_server.public_ip]

}


resource "aws_route53_record" "grafana" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.grafana_sub_domain
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.monitoring_server.public_ip]
 
}

resource "aws_route53_record" "es01" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.es01_sub_domain
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.elasticsearch_server01.public_ip]

}


resource "aws_route53_record" "es02" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.es02_sub_domain
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.elasticsearch_server02.public_ip]

}

resource "aws_route53_record" "es03" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.es03_sub_domain
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.elasticsearch_server03.public_ip]

}

resource "aws_route53_record" "kibana" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.kibana_sub_domain
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.kibana_server.public_ip]

}

resource "aws_route53_record" "sonarqube_server" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.sonarqube_sub_domain
  type    = var.aws_route53_record_type
  ttl     = 300
  records = [aws_instance.sonarqube_server.public_ip]

}




# статический IP адрес
#resource "aws_eip" "my_static_ip" {
#  instance = aws_instance.my_gitlab_runner_server.id
#  tags = {
#    Name  = "Server with gitlab runner"
#  }
#}


# Запускаем инстанс
#resource "aws_instance" "my_gitlab_runner_server" {
#  ami                    = data.aws_ami.ubuntu.id
#  instance_type          = "t2.micro"
#  vpc_security_group_ids = [aws_security_group.my_gitlab_runner_server.id]
#  user_data = file("user_data.sh")
#  key_name = var.ssh_key_name
#  tags = {
#    AMI =  "${data.aws_ami.ubuntu.id}"
#    Name  = "Server_for_gitlab_runner"
#    CM = "gitlab runner"
#  }
#
#  lifecycle {
#    create_before_destroy = true
#  }
#
#}

# Политики безопасности для настроек доступа к серверу 
resource "aws_security_group" "my_gitlab_runner_server" {
  name        = "Servers Security Group for gitlab runner"
  description = "Security group for accessing traffic to our Server with gitlab runner"

  dynamic "ingress" {
    for_each = ["80", "22", "81"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Server SecurityGroup for gitlab-runner"
  }
}

# Выведем IP адрес сервера
#output "my_server_with_gitlab_runner_ip" {
#  description = "Elatic IP address assigned to server with gitlab runner"
#  value       = aws_eip.my_static_ip.public_ip
#}


# Запускаем инстанс мониторинга
resource "aws_instance" "monitoring_server" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.monitoring_server.id]
  user_data = file("user_data.sh")
  key_name = var.ssh_key_name
  
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Server_for_monitoring"
    CM = "monitoring"
  }

  lifecycle {
    create_before_destroy = true
  }
}

#Политики безопасности для настроек доступа к серверу мониторинга
resource "aws_security_group" "monitoring_server" {
  name        = "Server for monitoring"
  description = "Security group for accessing traffic to our Monitoring Server"
  dynamic "ingress" {
    for_each = ["80", "22", "81", "5432", "9000", "9090", "9100", "3000", "8500", "8600", "8300","8301", "8302", "8428"] 
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Server SecurityGroup for monitoring"
  }
}

# Consul Instances
resource "aws_instance" "consul_instance" {
  count                  = 3
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.consul_server.id]
  user_data              = file("user_data.sh")
  key_name               = var.ssh_key_name

  tags = {
    Name = "Consul_Server"
    CM   = "consul"
  }
}

# Security Group for Consul Servers
resource "aws_security_group" "consul_server" {
  name        = "Consul Servers"
  description = "Security group for Consul servers"

  dynamic "ingress" {
    for_each = ["80", "22", "8500", "8600", "8300","8301", "8302"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "ingress" {
    for_each = ["8600", "8302"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "udp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Consul Server Security Group"
  }
}

# Запускаем инстанс для Consul-template
resource "aws_instance" "consul-template_server" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.consul_server.id]
  user_data = file("user_data.sh")
  key_name = var.ssh_key_name
  
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name  = "Server_for_Consul-template"
    CM = "consul-template"
  }

  lifecycle {
    create_before_destroy = true
  }
}



# Запускаем инстансы elasticearh
resource "aws_instance" "elasticsearch_server01" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.elastic_server.id]
  user_data = file("user_data.sh")
  key_name = var.ssh_key_name
  
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name = "es01"
    CM = "elastic01"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "elasticsearch_server02" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.elastic_server.id]
  user_data = file("user_data.sh")
  key_name = var.ssh_key_name
  
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name = "es02"
    CM = "elastic02"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "elasticsearch_server03" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.elastic_server.id]
  user_data = file("user_data.sh")
  key_name = var.ssh_key_name
  
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name = "es03"
    CM = "elastic03"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "kibana_server" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.elastic_server.id]
  user_data = file("user_data.sh")
  key_name = var.ssh_key_name
  
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name = "kibana"
    CM = "kibana"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_instance" "sonarqube_server" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  vpc_security_group_ids = [aws_security_group.monitoring_server.id]
  user_data = file("user_data.sh")
  key_name = var.ssh_key_name
  
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    Name = "sonarqube"
    CM = "sonarqube"
  }

  lifecycle {
    create_before_destroy = true
  }
}


#Политики безопасности для настроек доступа к elastic и kibana
resource "aws_security_group" "elastic_server" {
  name        = "Server for elastic"
  description = "Security group for accessing traffic to our Elasticsearch Server"
  dynamic "ingress" {
    for_each = ["80", "22", "443", "5044", "5601", "9200", "9300"] 
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Server SecurityGroup for elastic"
  }
}