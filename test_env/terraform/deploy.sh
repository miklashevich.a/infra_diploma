#!/bin/bash

# Extract private IP address
PRIVATE_IP=$(curl -s http://169.254.169.254/latest/meta-data/local-ipv4)


# Set your desired values
SERVICE_NAME="backend"
SERVICE_PORT="8080"

# Consul configuration file path
CONSUL_CONFIG="/etc/consul/config.json"

# Restart Consul with a new node ID
sudo systemctl stop consul
sudo rm -rf /opt/consul/*   # Remove Consul data directory for a clean start
sudo sed -i 's/"advertise_addr": ".*"/"advertise_addr": "'"$PRIVATE_IP"'"/' "$CONSUL_CONFIG"
sudo sed -i 's/"advertise_addr_wan": ".*"/"advertise_addr_wan": "'"$PRIVATE_IP"'"/' "$CONSUL_CONFIG"
sudo sed -i 's/"bind_addr": ".*"/"bind_addr": "'"$PRIVATE_IP"'"/' "$CONSUL_CONFIG"
sudo systemctl start consul


# Register the service
consul services register -name "$SERVICE_NAME" -port "$SERVICE_PORT" 


sleep 10

consul_kv_path="app/release_version"
release_version=$(consul kv get "$consul_kv_path")

if [ -z "$release_version" ]; then
    release_version="latest"
    echo "Release version not found in Consul. Setting release_version to 'latest'."
else
    echo "Deploying app with release version: $release_version"
fi

export RELEASE_VERSION="$release_version"
compose_file="/home/ubuntu/diploma/docker-compose.yaml"
sed -i "s|image: registry.gitlab.com/miklashevich.a/service/main/skillbox_app:latest|image: registry.gitlab.com/miklashevich.a/service/main/skillbox_app:$RELEASE_VERSION|" "$compose_file"
systemctl restart skillbox-app