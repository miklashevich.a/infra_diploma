variable "region" {
  default     = "eu-central-1"
}

variable "ami_owners" {
  type = list  
  default     = ["099720109477"]
}

variable "ami_filter_name" {
  default     = "name"
}
variable "ami_filter_values" {
    type = list
  default     = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
}


variable "instance_type" {
  default     = "t3.micro"
}

variable "allow_ports" {
  description = "List of ports to open for instance"
  type        = list
  default     = ["22", "80", "443", "5044", "8080", "8300", "8301", "8302", "8500", "8600"]
}

variable "domain_name" {
    description = "domain name for Route 53"
    default     = "miklashevich.online"  
}

variable "aws_route53_record_type" {
    description = "type of record for Route 53"
    default     = "A"  
}

variable "monitoring_sub_domain" {
    description = "sub domain name for monitoring"
    default     = "monitoring"  
}

variable "grafana_sub_domain" {
    description = "sub domain name for grafana"
    default     = "grafana"  
}

variable "es01_sub_domain" {
    description = "sub domain name for elasticearh01"
    default     = "es01"  
}

variable "es02_sub_domain" {
    description = "sub domain name for elasticearh02"
    default     = "es02"  
}

variable "es03_sub_domain" {
    description = "sub domain name for elasticearh03"
    default     = "es03"  
}

variable "kibana_sub_domain" {
    description = "sub domain name for kibana"
    default     = "kibana"  
}

variable "sonarqube_sub_domain" {
    description = "sub domain name for sonarqube"
    default     = "sonarqube"  
}



variable "ttl" {
    description = "TTL value for NS DNS records"
    default     = 172800 
}


variable "ssh_key_name" {
    default = "andrew_HP"
}

variable "aws_security_group_name" {
    default = "Dynamic Security Group"  
}

variable "aws_security_group_tag_name" {
    default = "Web access for Application"
}

variable "aws_security_group_ingress_protocol" {
    default = "tcp"
}

variable "aws_security_group_egress_protocol" {
    default = "-1"
}

variable "security_group_ingress_cidr_blocks" {
    type    = list
    default = ["0.0.0.0/0"]
}

variable "security_group_egress_cidr_blocks" {
    type    = list
    default = ["0.0.0.0/0"]
}

variable "autoscaling_group_min_size" {
    default = "1"
}

variable "autoscaling_group_max_size" {
    default = "3"
}

variable "autoscaling_group_min_elb_capacity" {
    default = "1"
}

variable "autoscaling_group_health_check_type" {
    default = "ELB"
}

variable "autoscaling_group_dynamic_tag_name" {
    default = "App_Server_in_ASG"
}

variable "aws_elb_name" {
    default = "ELB"
}

variable "aws_elb_tag_name" {
    default = "ELB"
}

