#!/bin/bash

# Function to get CPU utilization
get_cpu_utilization() {
  # Get the CPU utilization percentage with decimal values
  cpu_utilization=$(top -bn1 | awk '/Cpu\(s\)/ {print $2}')

  # Convert the percentage to an integer
  cpu_utilization=$(printf "%.0f" "$cpu_utilization")

  echo "$cpu_utilization"
}

# Threshold for considering the system as unhealthy
unhealthy_threshold=70

# Get CPU utilization
cpu_usage=$(get_cpu_utilization)

# Check if CPU utilization is below the unhealthy threshold
if [ "$cpu_usage" -lt "$unhealthy_threshold" ]; then
  echo "0"  # Healthy
else
  echo "2"  # Unhealthy
fi
