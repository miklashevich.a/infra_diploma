# Short description 

The easiest way for configure all instances is run the script [run_plybooks.sh] which launch all needed roles.
If needed to reconfigure some inctances it is posssible to launch separate roles using command (example is below):

```
ansible-playbook install_consul

```



## Preconditions

Before run the script or ansible-playbook:
- [ ] set up needed variables (in corresponding roles)
- [ ] give script execute permissions (chmod +x run_playbooks.sh), and then run it (./run_playbooks.sh)





