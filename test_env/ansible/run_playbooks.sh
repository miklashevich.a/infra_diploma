#!/bin/bash

# List your playbooks in the order you want them to run
PLAYBOOKS=(
    "./install_consul.yaml"
    "./install_docker.yaml"
    "./install_prometheus.yaml"
    "./install_node-exporter.yaml"
    "./install_promtail.yaml"
    "./install_victoria-metrics.yaml"
    "./install_blackbox-exporter.yaml"
    "./install_sonarqube.yaml"
    "/ansible/ELK_Ansible_Playbook/playbook_with_elk_roles.yml"

)

# Loop over the playbook array
for playbook in "${PLAYBOOKS[@]}"; do
    echo "Running playbook: $playbook"
    ansible-playbook $playbook
done

echo "All playbooks have been run."
