#!/bin/bash

# Update package lists and install NGINX
sudo apt update -y
sudo apt install nginx -y

# Create NGINX configuration file
sudo tee /etc/nginx/nginx.conf > /dev/null <<EOF
http {
    upstream backend {
        least_conn;
        server app1-load-balancer-url;
        server app2-load-balancer-url;
    }

    server {
        listen 80;

        location / {
            proxy_pass http://backend;
        }
    }
}
EOF

# Start NGINX service
sudo systemctl start nginx

# Add private key
echo "$PRIVATE_KEY" | sudo tee /home/ubuntu/.ssh/id_rsa
sudo chmod 600 /home/ubuntu/.ssh/id_rsa