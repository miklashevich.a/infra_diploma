#!/bin/bash

# Update package lists and install NGINX using apt
sudo apt update -y
sudo apt install -y nginx

# Create NGINX configuration file
sudo tee /etc/nginx/nginx.conf > /dev/null <<EOF
events {
    worker_connections 1024;
}

http {
    server {
        listen 80;

        location / {
            proxy_pass http://10.0.13.230;
        }
    }
}
EOF

# Start NGINX service
sudo systemctl start nginx
