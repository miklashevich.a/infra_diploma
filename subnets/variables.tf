variable "region" {
  default     = "us-east-1"
}

variable "ami_owners" {
  type = list  
  default     = ["099720109477"]
}

variable "ami_filter_name" {
  default     = "name"
}
variable "ami_filter_values" {
    type = list
  default     = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
}


variable "instance_type" {
  default     = "t3.micro"
}


variable "ssh_key_name" {
    default = "andrew_HP"
}

variable "private_key" {
  description = "Private key for accessing resources"
  type        = string
}
