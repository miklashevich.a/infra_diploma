resource "aws_vpc" "my_vpc" {
  cidr_block = "10.0.0.0/16"  

  tags = {
    Name = "MyVPC"  
  }
}


data "aws_availability_zones" "available" {}

# Create an internet gateway
resource "aws_internet_gateway" "my_igw" {
  vpc_id = aws_vpc.my_vpc.id
}

# Create DMZ subnet
resource "aws_subnet" "dmz_subnet" {
  availability_zone = data.aws_availability_zones.available.names[0]
  vpc_id                  = aws_vpc.my_vpc.id
  cidr_block              = "10.0.11.0/24"
  map_public_ip_on_launch = true

  tags = {
    Name = "DMZSubnet"
  }
}

# Create public route table for DMZ subnet
resource "aws_route_table" "dmz_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "DMZ Route Table"
  }
}

resource "aws_route" "dmz_route_to_igw" {
  route_table_id         = aws_route_table.dmz_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my_igw.id
}

resource "aws_route_table_association" "dmz_route_table_association" {
  subnet_id      = aws_subnet.dmz_subnet.id
  route_table_id = aws_route_table.dmz_route_table.id
}

# Create security group for DMZ

resource "aws_security_group" "dmz_security_group" {
  name        = "DMZSecurityGroup"
  description = "Allow inbound traffic from DMZ to private subnets"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow traffic from anywhere
  }


  ingress {
    from_port       = 22  # SSH port
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]  # Allow SSH traffic from anywhere
  }
  
   # Allow outbound HTTP and HTTPS traffic for apt update
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTP traffic
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTPS traffic
  }

  egress {
    from_port       = 22  # SSH port
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]  # Allow SSH 
  }

}


# Create subnets for App1 and App2

resource "aws_subnet" "private_app1_subnet" {
  availability_zone = data.aws_availability_zones.available.names[0]
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.12.0/24"

  tags = {
    Name = "private_app1_subnet"
  }
}

resource "aws_subnet" "private_app2_subnet" {
  availability_zone = data.aws_availability_zones.available.names[0]
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.22.0/24"

  tags = {
    Name = "private_app2_subnet"
  }
}

# Create subnets for DB1 and DB2

resource "aws_subnet" "private_db1_subnet" {
  availability_zone = data.aws_availability_zones.available.names[0]
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.13.0/24"

  tags = {
    Name = "private_db1_subnet"
  }
}

resource "aws_subnet" "private_db2_subnet" {
  availability_zone = data.aws_availability_zones.available.names[0]
  vpc_id            = aws_vpc.my_vpc.id
  cidr_block        = "10.0.23.0/24"

  tags = {
    Name = "private_db2_subnet"
  }
}

# Create security groups for App1 and App2
resource "aws_security_group" "app1_sg" {
  name        = "App1SecurityGroup"
  description = "Allow traffic from App1 to DB1 and DMZ on port 80"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks     = [aws_subnet.private_app2_subnet.cidr_block]   # Allow communication within private subnet APP2
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.dmz_security_group.id]  # Allow communication with DMZ on port 80
  }

  
  ingress {
    from_port       = 22  
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]  # Allow SSH traffic from anywhere
  }
   # Allow outbound HTTP traffic
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTP traffic  
  }
    # Allow outbound SSH
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTP traffic
  }

  # Allow outbound to APP2 subnet
  egress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks     = [aws_subnet.private_app2_subnet.cidr_block]   # Allow communication within private subnet APP2
  }

  # Allow outbound to DB1 subnet
  egress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks     = [aws_subnet.private_db1_subnet.cidr_block]   # Allow communication within private subnet DB1
  }

}

resource "aws_security_group" "app2_sg" {
  name        = "App2SecurityGroup"
  description = "Allow traffic from App2 to DB2 and DMZ on port 80"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks     = [aws_subnet.private_app1_subnet.cidr_block]  # Allow communication within private subnet APP1
  }

  ingress {
    from_port       = 80
    to_port         = 80
    protocol        = "tcp"
    security_groups = [aws_security_group.dmz_security_group.id]  # Allow communication with DMZ on port 80
  }


  ingress {
    from_port       = 22 
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]  # Allow SSH traffic from anywhere
  }

  # Allow outbound HTTP traffic
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTP traffic
  }

  # Allow outbound SSH
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTP traffic
  }

  # Allow outbound to APP1 subnet
  egress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks     = [aws_subnet.private_app1_subnet.cidr_block]   # Allow communication within private subnet APP1
  }

  # Allow outbound to DB2 subnet
  egress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks     = [aws_subnet.private_db2_subnet.cidr_block]   # Allow communication within private subnet DB2
  }


}

# Create security groups for DB1 and DB2
resource "aws_security_group" "db1_sg" {
  name        = "DB1SecurityGroup"
  description = "Allow traffic from App1 to DB1"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [aws_security_group.app1_sg.id]  # Allow traffic from App1
  }


  ingress {
    from_port       = 22  # SSH port
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]  # Allow SSH traffic from anywhere
  }
  # Allow outbound HTTP traffic
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTP traffic
}
}

resource "aws_security_group" "db2_sg" {
  name        = "DB2SecurityGroup"
  description = "Allow traffic from App2 to DB2"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [aws_security_group.app2_sg.id]  # Allow traffic from App2
  }


  ingress {
    from_port       = 22  # SSH port
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks     = ["0.0.0.0/0"]  # Allow SSH traffic from anywhere
  }
  # Allow outbound HTTP traffic
  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow outbound HTTP traffic
}
}

# Create Load Balancer for DB1
resource "aws_elb" "db1_lb" {
  name               = "db1-lb"
  subnets            = [aws_subnet.private_db1_subnet.id]

  listener {
    instance_port     = 80
    instance_protocol = "HTTP"
    lb_port           = 80
    lb_protocol       = "HTTP"
  }

  health_check {
    target              = "HTTP:80/healthcheck"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
  }

  instances = [aws_instance.db1_instance.id]
}



# Create Load Balancer for DB2
resource "aws_elb" "db2_lb" {
  name               = "db2-lb"
  subnets            = [aws_subnet.private_db2_subnet.id]
  
  listener {
    instance_port     = 80
    instance_protocol = "HTTP"
    lb_port           = 80
    lb_protocol       = "HTTP"
  }

  health_check {
    target              = "HTTP:80/healthcheck"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
  }

  instances = [aws_instance.db2_instance.id]
}



data "aws_ami" "ubuntu" {
  owners      = var.ami_owners
  most_recent = true
  filter {
    name   = var.ami_filter_name
    values = var.ami_filter_values
  }
}


# Create instances 

resource "aws_instance" "dmz_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.dmz_subnet.id
  key_name      = var.ssh_key_name
  vpc_security_group_ids = [aws_security_group.dmz_security_group.id]

  user_data = <<-EOF
                #!/bin/bash
                
                # Update package lists and install NGINX
                sudo apt update -y
                sudo apt install nginx -y

                # Create NGINX configuration file
                sudo tee /etc/nginx/nginx.conf > /dev/null <<EOG
                events {
                   worker_connections 1024;
                }

                http {
                  upstream backend {
                    least_conn;
                    server 10.0.12.31;
                    server 10.0.22.178;
                  }

                  server {
                     listen 80;

                    location / {
                      proxy_pass http://backend;
                    }
                  }
                }

                EOG

                # Start NGINX service
                sudo systemctl start nginx

                export PRIVATE_KEY="${var.private_key}"
                # Add private key
                echo "$PRIVATE_KEY" | sudo tee /home/ubuntu/.ssh/id_rsa
                sudo chown ubuntu:ubuntu /home/ubuntu/.ssh/id_rsa
                sudo chmod 600 /home/ubuntu/.ssh/id_rsa
              EOF

  tags = {
    Name = "nginx-lb"
  }
}







resource "aws_instance" "app1_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.private_app1_subnet.id
  key_name      = var.ssh_key_name
  vpc_security_group_ids  = [aws_security_group.app1_sg.id]
  
  user_data = file("user_data1.sh")

  tags = {
    Name = "app1-nginx"
  }

}

resource "aws_instance" "app2_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.private_app2_subnet.id
  key_name      = var.ssh_key_name
  vpc_security_group_ids  = [aws_security_group.app2_sg.id]
  
  user_data = file("user_data2.sh")

  tags = {
    Name = "app2-nginx"
  }

}




resource "aws_instance" "db1_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.private_db1_subnet.id
  key_name      = var.ssh_key_name
  vpc_security_group_ids = [aws_security_group.db1_sg.id]
  
  user_data = file("user_data3.sh")
  tags = {
    Name  = "db1_instance"
  }
}


resource "aws_instance" "db2_instance" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = var.instance_type
  subnet_id     = aws_subnet.private_db2_subnet.id
  key_name      = var.ssh_key_name
  vpc_security_group_ids = [aws_security_group.db2_sg.id]
  
  user_data = file("user_data4.sh")
  tags = {
    Name  = "db2_instance"
  }
}






# Create a route table for App1 subnet
resource "aws_route_table" "app1_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "App1 Route Table"
  }
}

# Associate the App1 subnet with the App1 route table
resource "aws_route_table_association" "app1_route_table_association" {
  subnet_id      = aws_subnet.private_app1_subnet.id
  route_table_id = aws_route_table.app1_route_table.id
}

# Create a route table for App2 subnet
resource "aws_route_table" "app2_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "App2 Route Table"
  }
}

# Associate the App2 subnet with the App2 route table
resource "aws_route_table_association" "app2_route_table_association" {
  subnet_id      = aws_subnet.private_app2_subnet.id
  route_table_id = aws_route_table.app2_route_table.id
}

# Create a route table for DB1 subnet
resource "aws_route_table" "db1_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "DB1 Route Table"
  }
}

# Associate the DB1 subnet with the DB1 route table
resource "aws_route_table_association" "db1_route_table_association" {
  subnet_id      = aws_subnet.private_db1_subnet.id
  route_table_id = aws_route_table.db1_route_table.id
}

# Create a route table for DB2 subnet
resource "aws_route_table" "db2_route_table" {
  vpc_id = aws_vpc.my_vpc.id

  tags = {
    Name = "DB2 Route Table"
  }
}

# Associate the DB2 subnet with the DB2 route table
resource "aws_route_table_association" "db2_route_table_association" {
  subnet_id      = aws_subnet.private_db2_subnet.id
  route_table_id = aws_route_table.db2_route_table.id
}

# Create a NAT gateway
resource "aws_eip" "nat" {
  domain = "vpc"
}

resource "aws_nat_gateway" "nat_gateway" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.dmz_subnet.id
  depends_on    = [aws_internet_gateway.my_igw]
}

# Add a route to the route tables for APP1, APP2, DB1, and DB2
resource "aws_route" "app1_nat_route" {
  route_table_id         = aws_route_table.app1_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gateway.id
}

resource "aws_route" "app2_nat_route" {
  route_table_id         = aws_route_table.app2_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gateway.id
}

resource "aws_route" "db1_nat_route" {
  route_table_id         = aws_route_table.db1_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gateway.id
}

resource "aws_route" "db2_nat_route" {
  route_table_id         = aws_route_table.db2_route_table.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.nat_gateway.id
}