#!/bin/bash

# Install NGINX
sudo apt update
sudo apt install -y nginx

# Modify NGINX configuration
echo '
events {
    worker_connections 1024;
}

http {
    server {
        listen 80;

        location / {
            return 200 "DB2\n";
        }
    }
}

' | sudo tee /etc/nginx/nginx.conf

# Restart NGINX to apply changes
sudo systemctl restart nginx
