# network_secutity

In this repository 1 VPC and 5 subnets will be created using terraform

## Getting started

- Install terraform on your local machine
- Clone this repository 


## Add your files

- Create file with name [sensitive.tfvars] which should contain your ssh private key (examle is below)

```
private_key = <<-EOF
-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
wertyuiopertyuiop[]tyuio
KRkQkC7Br3XtMAAAAJYW5kcmV3QEhQAQI=
-----END OPENSSH PRIVATE KEY-----
EOF
```

## Run terraform 

- [terraform plan -var-file="sensitive.tfvars" ] 
after run this command all needed infrastructure will be installed in AWS (including inctances, VPC, subnets, routing tables, security groups, internet gateway, NAT gateway etc.)

## Configure load-balancers

- [ ] [make ssh login to the DMZ instance and change ip addreses for nginx config in the /etc/nginx/nginx.conf  according to the existing APP1 and APP2 IP addresses]
- [ ] [ make restart nginx service]
- [ ] [make ssh login to the APP1 instance and change ip address for nginx config in the /etc/nginx/nginx.conf  according to the existing DB1 IP address]
- [ ] [ make restart nginx service]
- [ ] [make ssh login to the APP2 instance and change ip address for nginx config in the /etc/nginx/nginx.conf  according to the existing DB1 IP address]
- [ ] [ make restart nginx service]

