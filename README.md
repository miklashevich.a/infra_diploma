# Short description of skillbox-diploma project

In this repository is skillbox diploma project

Using terraform and Ansible we will create and configure infractructure for the following tasks:

- [ ] **Create Service Discovery and monitoring**
- [ ] **Autoscaling app instances and getting release version using Consul KV store**
- [ ] **Improve monitoring using Victoria metrics**
- [ ] **Create grafana dashboards with http code responses**
- [ ] **Deploy ELK stack for logs analysis** 
- [ ] **Deploy SonarQube for code analysis and integrate it to the gitlab ci pipline for service**
- [ ] **Create separate infrustructure with Network Secutity requirements.**


- Instances will be created usung Terraform (open folder /test_env/terraform for the next instructions)
- Instances will be configured using Ansible roles (open folder /test_env/ansible for the next instructions)
- For creating infrastructure for security network (open folder /subnets for the next instructions)
- For CI will be used repository [service](https://gitlab.com/miklashevich.a/service)


## Preconditions

To creatingg infrustructure it's necessary:
- [ ] install [terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- [ ] install [ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [ ] install [ansible-galaxy collection amazon](https://docs.ansible.com/ansible/latest/collections/amazon/aws/aws_ec2_inventory.html#parameter-aws_access_key)
- [ ] have an access to create AWS inctances



## Steps 
- [ ] [Make git pull]
- [ ] [Change directory to "terraform"]
- [ ] [In the "terraform" folder create a file 'provider.tf'] 
with the following code:

```
provider "aws" {
  access_key = "insert your access_key"
  secret_key = "insert your secret_key"
  region     = var.region
}
```
- [ ] [Make terraform init]
When running the "terraform init" command you have to add -backend-config options for your credentials (aws keys). So your command should look like:
```
terraform init -backend-config="access_key=insert your access_key" -backend-config="secret_key=insert your secret_key"

```
- [ ] [To launch creating instances, run the command:]

```
terraform apply

```

- [ ] [After terraform created instances, it is necessary of instances configuration:]
The configuration will be done using the ansible roles, here the steps for configuration:

1. **"ansible amazon.aws.aws_ec2" plugin should be launched** (for get IP addresses from AWS) 
- [ ] [In the "ansible" folder create a file 'aws_ec2.yml'] 
with the following code:

```
plugin: amazon.aws.aws_ec2

aws_access_key: insert your access_key
aws_secret_key: insert your secret_key

keyed_groups:
  - key: tags.Name
filters:
  instance-state-name: running
compose:
ansible_host: public_ip_address

```

- [ ] [Run the command:]

```
ansible-inventory --graph

```

- [ ] [As a result we can launch ansible roles using terraform instances tags]



